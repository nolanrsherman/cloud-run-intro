# We can use a image that has the dependencies we need for our node app
FROM node:current-alpine

# Copy all the files from our app into a directory
COPY ./* /my-app
# Set the working directory to the one our app files are in
WORKDIR /my-app

# Our app listens on port 3000, so we need to expose it.
EXPOSE 3000

# This is the command to run when our container starts
CMD ["npm", "run", "start"]